package nerdery

import org.wasabi.interceptors.Interceptor
import org.wasabi.protocol.http.Request
import org.wasabi.protocol.http.Response
import org.wasabi.protocol.http.StatusCodes
import org.wasabi.routing.InterceptOn
import java.util.concurrent.TimeUnit


/**
 * Created by rvanbelk on 7/4/16.
 */
object Rest {

    fun start() {
        server.post("/queue/insert/:item/:target", {
            val item = request.routeParams["item"]
            val target = request.routeParams["target"]
            try {
                if (item != null && target != null)response.send("Insert successful = " + Queueman.insert(item, target))
                else response.send("No item or target")
            } catch (e : Exception) {
                response.send("Unable to insert to queue")
            }
        })
        server.post("/queue/remove/:item/:target", {
            val item = request.routeParams["item"]
            val target = request.routeParams["target"]
            try {
                if (item != null && target != null)response.send("Remove successful = " + Queueman.remove(item, target))
                else response.send("No item or target")
            } catch (e : Exception) {
                response.send("Unable to remove from queue")
            }
        })
        server.get("/queue/status", {
            response.send(queue)
        })
        server.post("/thread/toggleHitman", {
            try {
                response.send("TargetType = " + toggleHitman())
            } catch (e : Exception) {
                response.send("Unable to toggle hitman")
            }
        })
        server.post("/thread/:thread/shutdown", {
            val thread = request.routeParams["thread"]
            try {
                shutdownThread(thread)
                val (hitman, points, watchman) = threadStatus()
                response.send("Shutdown thread for $thread: hitman = $hitman, points = $points, watchman = $watchman")
            } catch (e : Exception) {
                response.send("Shutdown of $thread failed")
            }
        })
        server.post("/thread/:thread/startup", {
            val thread = request.routeParams["thread"]
            try {
                startupThread(thread)
                val (hitman, points, watchman) = threadStatus()
                response.send("Startup thread for $thread: hitman = $hitman, points = $points, watchman = $watchman")
            } catch (e : Exception) {
                response.send("Startup of $thread failed")
            }
        })
        server.get("/thread/status", {
            try {
                val (hitman, points, watchman) = threadStatus()
                response.send("hitman = $hitman, points = $points, watchman = $watchman")
            } catch (e : Exception){
                response.send("Theads down")
            }

        })
        server.intercept(MyInterceptor(), "*", InterceptOn.PreRequest)
        server.start()
    }

    private fun toggleHitman() : TargetType{
        if (targetType == TargetType.FRIENDLY) {
            targetType = TargetType.VILLAIN
        } else {
            targetType = TargetType.FRIENDLY
        }
        return targetType
    }

    public fun shutdownThread (thread : String?) {
        if (thread == "hitman") {
            hitmanThread?.cancel(true)
        } else if (thread == "points") {
            pointThread?.cancel(true)
        } else if (thread == "watchman" ) {
            watchmanThread?.cancel(true)
        } else {
            watchmanThread?.cancel(true)
            hitmanThread?.cancel(true)
            pointThread?.cancel(true)
        }
    }

    public fun startupThread(thread : String?) {
        val (hitman, points, watchman) = threadStatus()
        if (thread == "hitman" && !hitman) {
            hitmanThread = executor.scheduleWithFixedDelay(Hitman, 0, 1, TimeUnit.MINUTES)
        } else if (thread == "points" && !points) {
            pointThread = executor.scheduleWithFixedDelay(Pointman, 0, 1, TimeUnit.SECONDS)
        } else if (thread == "watchman" && !watchman) {
            watchmanThread = executor.scheduleWithFixedDelay(Watchman, 0, 2, TimeUnit.MINUTES)
        } else {
            if (!points) executor.scheduleWithFixedDelay(Pointman, 0, 1, TimeUnit.SECONDS)
            if (!hitman) executor.scheduleWithFixedDelay(Hitman, 0, 1, TimeUnit.MINUTES)
            if (!watchman) executor.scheduleWithFixedDelay(Watchman, 0, 2, TimeUnit.MINUTES)
        }
    }

    private fun threadStatus() : Triple<Boolean, Boolean, Boolean> {
        return (Triple(!hitmanThread!!.isCancelled, !pointThread!!.isCancelled, !watchmanThread!!.isCancelled))
    }
}

class MyInterceptor : Interceptor() {
    override fun intercept(request: Request, response: Response): Boolean {
        var executeNext = false
        val apiKey = request.rawHeaders["apikey"]
        if (apiKey != "dontbeanahole") {
            response.setStatus(StatusCodes.Unauthorized)
            response.send("Sorry")
        } else {
            executeNext = true
        }
        return executeNext
    }
}