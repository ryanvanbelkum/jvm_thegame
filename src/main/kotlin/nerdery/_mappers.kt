package nerdery

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by rvanbelk on 7/3/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class GameObj @JsonCreator constructor(
        @JsonProperty("Messages") val messages: List<String>,
        @JsonProperty("Item") val item: GameItem?,
        @JsonProperty("Effects") val effects: Set<String>) {
}
@JsonIgnoreProperties(ignoreUnknown = true)
data class GameItem @JsonCreator constructor(@JsonProperty("Fields") val fields: List<GameItemFields>) {
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GameItemFields @JsonCreator constructor(
        @JsonProperty("Id") val id: String,
        @JsonProperty("Name") val name: String,
        @JsonProperty("Rarity") val rarity: String,
        @JsonProperty("Description") val description: String) {
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Player @JsonCreator constructor(
        @JsonProperty("PlayerName") val playerName: String,
        @JsonProperty("Pointman") val points: Int,
        @JsonProperty("Effects") val effects: Set<String>) {
}

data class QueueItem(val item: ItemType?, val target: String)
