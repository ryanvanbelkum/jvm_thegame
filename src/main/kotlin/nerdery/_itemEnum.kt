package nerdery

import java.util.*

/**
 * BORROWED FROM RYAN EVANS
 */
enum class ItemType(
        val itemName: String,
        val targetType: TargetType) {

    BANANA_PEEL("Banana Peel", TargetType.VILLAIN),
    BIGGS("Biggs", TargetType.FRIENDLY),
    BLUE_SHELL("Blue Shell", TargetType.VILLAIN),
    BO_JACKSON("Bo Jackson", TargetType.FRIENDLY),
    BOX_OF_BEES("Box of Bees", TargetType.NONE),
    BUFFALO("Buffalo", TargetType.FRIENDLY),
    BULLET_BILL("Bullet Bill", TargetType.VILLAIN),
    BUSTER_SWORD("Buster Sword", TargetType.VILLAIN),
    CARBUNCLE("Carbuncle", TargetType.VILLAIN),
    CARDBOARD_BOX("Cardboard Box", TargetType.NONE),
    CHARIZARD("Charizard", TargetType.VILLAIN),
    CHOCOBO("Chocobo", TargetType.FRIENDLY),
    CROWBAR("Crowbar", TargetType.VILLAIN),
    DA_DA_DA("Da Da Da Da Daaa Da DAA", TargetType.FRIENDLY),
    FAT_GUYS("Fat Guys", TargetType.VILLAIN),
    FIRE_FLOWER("Fire Flower", TargetType.VILLAIN),
    FUS_RO_DAH("Fus Ro Dah", TargetType.VILLAIN),
    GET_OVER_HERE("Get Over Here", TargetType.NONE),
    GOLD_RING("Gold Ring", TargetType.FRIENDLY),
    GOLDEN_GUN("Golden Gun", TargetType.VILLAIN),
    GREEN_SHELL("Green Shell", TargetType.VILLAIN),
    HADOUKEN("Hadouken", TargetType.VILLAIN),
    HARD_KNUCKLE("Hard Knuckle", TargetType.VILLAIN),
    HOLY_WATER("Holy Water", TargetType.VILLAIN),
    JIGGLYPUFF("Jigglypuff", TargetType.FRIENDLY),
    KONAMI("UUDDLRLRBA", TargetType.FRIENDLY),
    LEEROY_JENKINS("Leeroy Jenkins", TargetType.FRIENDLY),
//    LEISURE_SUIT("Leisure Suit", TargetType.VILLAIN),
    LUCKY_SEVENS("7777", TargetType.FRIENDLY),
    MASTER_SWORD("Master Sword", TargetType.VILLAIN),
    MOOGLE("Moogle", TargetType.FRIENDLY),
    MUSHROOM("Mushroom", TargetType.FRIENDLY),
    PANDORAS_BOX("Pandora's Box", TargetType.NONE),
    PIZZA("Pizza", TargetType.FRIENDLY),
    POKEBALL("Pokenball", TargetType.FRIENDLY),
    PORTAL_NUN("Portal Nun", TargetType.NONE),
    RED_CRYSTAL("Red Crystal", TargetType.VILLAIN),
    RED_SHELL("Red Shell", TargetType.VILLAIN),
    ROGER_WILCO("Roger Wilco", TargetType.NONE),
    RUSH("Rush The Dog", TargetType.VILLAIN),
    STAR("Star", TargetType.END),
    SPACE_HAMSTER("Miniature Giant Space Hamster", TargetType.VILLAIN),
    SPACE_INVADERS("Space Invaders", TargetType.VILLAIN),
    SPNKR("SPNKR", TargetType.VILLAIN),
    TANOOKI("Tanooki Suit", TargetType.FRIENDLY),
    TREASURE_CHEST("Treasure Chest", TargetType.FRIENDLY),
    WARTHOG("Warthog", TargetType.FRIENDLY),           // 2x pts
    VARIA("Varia Suit", TargetType.FRIENDLY),
    WEDGE("Wedge", TargetType.NONE);


    companion object {
        private val typesByItemName = values().associateBy { it.itemName }
        private val VALUES = values()
        private val SIZE = VALUES.size
        private val RANDOM = Random()

        fun randomItem(target : TargetType): ItemType? {
            val itemsByTarget = VALUES.groupBy { it.targetType == target }.get(true)
            val size = itemsByTarget?.size
            return itemsByTarget?.get(RANDOM.nextInt(size!!))
        }

        fun getEnumByItemName(itemName: String) : ItemType?{
            return typesByItemName.get(itemName)
        }
    }
}

enum class TargetType {
    FRIENDLY,
    VILLAIN,
    NONE,
    END
}