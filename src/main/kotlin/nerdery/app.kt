package nerdery

import java.util.concurrent.TimeUnit


fun main(args: Array<String>) {
    pointThread = executor.scheduleWithFixedDelay(Pointman, 0, 1, TimeUnit.SECONDS)
    hitmanThread = executor.scheduleWithFixedDelay(Hitman, 0, 1, TimeUnit.MINUTES)
    watchmanThread = executor.scheduleWithFixedDelay(Watchman, 0, 2, TimeUnit.MINUTES)

    Rest.start()
}