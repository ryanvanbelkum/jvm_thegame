package nerdery

/**
 * Created by rvanbelk on 7/3/16.
 */

object Queueman {

   fun insert(item : String, target : String): Boolean{
        try {
            val queueItem = QueueItem(ItemType.getEnumByItemName(item), target)
            if (queueItem.item != null && queueItem.target != null) queue.add(queueItem)
            else return false
            return true
        } catch (e: Exception) {
            println("Queueman, could not insert into queue! $e")
            return false
        }
    }

    fun remove(item : String, target : String): Boolean{
        try {
            val queueItem = QueueItem(ItemType.getEnumByItemName(item), target)
            queue.remove(queueItem)
            return true
        } catch (e: Exception) {
            println("Queueman, could not remove from queue! $e")
            return false
        }
    }

    fun retrieveNextItem(): QueueItem? {
        if (queue.isNotEmpty()) return queue.get(0)!!
        else return null
    }
}