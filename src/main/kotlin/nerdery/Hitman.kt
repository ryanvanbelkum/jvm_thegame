package nerdery

import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.result.Result

/**
 * Created by rvanbelk on 7/3/16.
 *
 * Hitman is sometimes destructive, sometimes friendly, and always loyal
 */

object Hitman : Runnable {

    var fromQueue : Boolean? = null
    override fun run() {
        try {
            val queueItem = Queueman.retrieveNextItem()
            var player : String?
            var item : String?
            if (queueItem == null) {
                fromQueue = false
                player = if (targetType == TargetType.VILLAIN) removeFriends(Watchman.leaderBoard())?.playerName!! else myself.playerName!!
                item = readItems(targetType).id
            } else {
                fromQueue = true
                player = queueItem.target
                item = queueItem.item!!.itemName
            }
            FuelManager.instance.baseHeaders = headers
            val (request, response, result) = Fuel.post("$nerderyurl/items/use/$item?target=$player").body("{}").responseString()
            result.fold({ d ->
                println(d)
            }, { err ->
                println("Hitman failed = ${err.response}")
            })
            removeItemAfterUse(item, player)
        } catch (e : Exception) {
            println("Hitman, don't die on me! $e")
        }
    }

    private fun removeItemAfterUse(item: String, target: String){
        if (fromQueue!!) {
            Queueman.remove(item, target)
        } else {
            deleteItems(item)
        }
    }

    private fun removeFriends(players : Array<Player>?) : Player? {
        var counter : Int = 0
        while (friends.contains(players?.get(counter)?.playerName)){
            counter++
        }
        return players?.get(counter)
    }
}