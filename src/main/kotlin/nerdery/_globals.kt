//ssh -v -i /Users/rvanbelk/Downloads/aws_ec2.pem ec2-user@ec2-52-38-180-28.us-west-2.compute.amazonaws.com

package nerdery

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.wasabi.app.AppServer
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture

/**
 * Created by rvanbelk on 7/3/16.
 */
val executor = Executors.newSingleThreadScheduledExecutor()
val mapper = jacksonObjectMapper()
val server = AppServer()
val myself : Player = Player("rvanbelk", 0, setOf())
val queue : MutableList<QueueItem?> = mutableListOf()
val headers = mapOf("apikey" to apikey, "content-length" to "0", "Content-Type" to "application/json", "Accept" to "application/json")

var pointThread : ScheduledFuture<*>? = null
var hitmanThread : ScheduledFuture<*>?= null
var watchmanThread : ScheduledFuture<*>?= null
var targetType = TargetType.FRIENDLY
var currentState : GameObj? = null
var errorCounter : Int = 0
