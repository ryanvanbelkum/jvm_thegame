package nerdery

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

var conn : Connection? = null
/**
 * Created by rvanbelk on 7/3/16.
 */
fun openMysqlConnection() {
    if (conn == null) {
        Class.forName(driver).newInstance()
        conn = DriverManager.getConnection("$dbUrl?user=$user&password=$password")
        println("Connected to the database")
    }
}

fun closeMysqlConnection() {
    conn?.close()
    println("Disconnected from database")
}

fun persist(state: GameObj): Boolean {
    val statement = "INSERT INTO items (`Urlid`, `Rarity`, `Description`, `Name`) " +
            "VALUES ('${state.item?.fields?.get(0)?.id}', '${state.item?.fields?.get(0)?.rarity}', '${state.item?.fields?.get(0)?.description?.replace("'", "")}', " +
            "'${state.item?.fields?.get(0)?.name}')"
    println(statement)
    try {
        openMysqlConnection()
        conn?.createStatement()?.executeUpdate(statement);
        return true
    } catch (e: SQLException) {
        println(e)
        return false
    }
}
fun readItems(target : TargetType) : GameItemFields {
    var item : ItemType? = ItemType.randomItem(target)
    while (currentState!!.effects.contains(item?.itemName) ) {
        item = ItemType.randomItem(target)
    }
    val statement = "SELECT * FROM items WHERE Name = '${item?.itemName}'"
    println(statement)
    val itemList : MutableList<GameItemFields> = mutableListOf()
    try {
        openMysqlConnection()
        val dbItem = conn?.createStatement()?.executeQuery(statement);
        while (dbItem!!.next()){
            itemList.add(GameItemFields(dbItem.getString("Urlid"), dbItem.getString("Name"), dbItem.getString("Rarity"), dbItem.getString("Description")))
        }
    } catch (e: SQLException) {
        println(e)
    }
    val returnItem = if (itemList.size > 0) itemList.get(0) else readItems(target)
    return returnItem
}

fun deleteItems(item : String) {
    val statement = "DELETE FROM items WHERE Urlid='$item'"
    println(statement)
    try {
        openMysqlConnection()
        conn?.createStatement()?.executeUpdate(statement);
    } catch (e: SQLException) {
        println(e)
    }
}