package nerdery

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.result.Result

/**
 * Created by rvanbelk on 7/3/16.
 */

object Watchman : Runnable {

    override fun run() {
        try {
            val leaderBoard = leaderBoard()
            val place : Int = leaderBoard?.indexOfFirst { it.playerName.equals(me) } ?: Int.MAX_VALUE//0 = 1st place
            if (place < 4 && place > -1){//go idle so that we don't get too high up on leadboard
                println("Watchman - Shutting down hitman and points.  You're in $place")
                Rest.shutdownThread("hitman")
                Rest.shutdownThread("points")
            } else {
                Rest.startupThread("hitman")
                Rest.startupThread("points")
            }
        } catch (e: Exception) {
            println("Watchman, don't die on me! $e")
        }
    }

    public fun leaderBoard() : Array<Player>? {
        var state : Array<Player>? = null
        FuelManager.instance.baseHeaders = headers
        val (request, response, result) = Fuel.get("$nerderyurl?page=0").body("{}").responseString()
        result.fold({ d ->
            try {
                state = mapper.readValue((result as Result.Success).value)
            } catch (e: Exception) {
                println("leadboard mapper crapped out = $e")
            }
        }, { err ->
            println("couldn't access leaderboard = $err")
        })
        return state
    }
}