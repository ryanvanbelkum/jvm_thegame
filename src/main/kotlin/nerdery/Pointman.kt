package nerdery

import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.result.Result

/**
 * Created by rvanbelk on 7/3/16.
 */

object Pointman : Runnable {

    override fun run() {
        try {
            FuelManager.instance.baseHeaders = headers
            val (request, response, result) = Fuel.post("$nerderyurl/points").body("{}").responseString()
            result.fold({ d ->
                try {
                    val state: GameObj = mapper.readValue(d)
                    currentState = state
                    println(state)
                    if (state.item != null) persist(state)
                } catch (e: Exception) {
                    println("Stop changing the spec = $e")
                }
            }, { err ->
                println("oh uhhh")
            })
        } catch (e: Exception) {
            println("Pointman, don't die on me! $e")
        }
    }
}