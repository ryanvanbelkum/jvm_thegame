# The Game

This repository represents my solution to the Nerdery JVM Challenge, "The Game"

## Running

To run the application, simply invoke the provided JAR like so:

    java -jar <path/to/project>/dist/the_game_jvm_challenge-1.0-all.jar

## Building

To build the application, use the provided `gradlew` tool:

    ./gradlew clean shadowJar